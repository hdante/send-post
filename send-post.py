#!/usr/bin/env python3

import sys, os, urllib.parse

url = sys.argv[1]
sessionid = sys.argv[2]
csrftoken = sys.argv[3]

def cook(arg):
    return urllib.parse.quote_plus(arg, safe='/=')

def base(url):
    s = urllib.parse.urlparse(url)
    return '%s://%s/' % (s.scheme, s.netloc)

referer = base(url)
data = '&'.join(map(cook, sys.argv[4:]))
cookies = 'sessionid=%s;csrftoken=%s' % (sessionid, csrftoken)
headers = 'X-CSRFToken: %s' % csrftoken

command = "curl %s -b '%s' -d '%s' -H '%s' -e %s -D /dev/stdout" % (
        url, cookies, data, headers, referer)

print(command)
os.system(command)
print()
